+++
title = "Reworking all my code to C"
date = 2023-09-27
draft = true
+++

I'm currently in the process of reworking all the code for
[Project Diggy](https://gitlab.com/Risgrynsgrot/diggy/)
to C. Partly as an exercise to appreciate modern features of newer languages, 
but mostly because I rant a lot about how I think C++ is a bloated language, and 
that the only real features I care about are templates. So to put that to the
test, I am moving all the code to C, changing out C++ specific libraries and 
remaking the things that don't have a C equivalent. It's going relatively
smoothly so far, but there are already some things that I miss a bit.

# namespaces

Namespaces are a really nice feature that I underappreciated before moving to C.
It makes separating code really easy and having multiple functions with the same
name can sometimes be very nice usability wise.

# naming

I'm still figuring out whether I'm more of a opening brace on the same line or 
new line,
pointer on value or type (T* foo or T *foo), snake_case or CamelCase 
kind of person when I work in C. 
I thought I would just want to do the same as I do in C++, but the
more I get into the rework, I realize that I look at things differently when
I don't have classes and namespaces for everything.

# classes

I've never been much of a inheritance person, but having functions directly 
connected to a class is sometimes very useful. I might go so far as to rename
the first parameter of the functions where I pass in a type to ```this```.
I do however not miss having trouble with binding in lambdas 
(not really an issue when there isn't any lambdas lol), or wanting to access a class function
that should have just been a free function. 
I am so far not missing private functions either,
something that I thought was very important before.

# struct definitions

the biggest thing that is annoying me right now is the divide in libraries between
using struct name {} vs typedef struct name_s {} name_t.
I thought I would be 100% on the typedef side, but after having some trouble
with forward declarations I'm not so sure anymore. In C++ it's very easy,
as you never need to think about having a struct or class keyword before you
use it in a function or instantiation. I'm considering following the [linux
guidelines](https://www.kernel.org/doc/html/latest/process/coding-style.html#typedefs) 
for this, to make it more clear that I'm using a struct.
