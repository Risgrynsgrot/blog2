+++
title = "C++ wasn't managable, so I moved to C"
date = 2024-04-05
draft = true
+++

Oh man, things have changed fast in just a few months...

I feel like I've had a lot of self doubt about what game I wanted to make 
the past few months. I abandoned [diggy](https://gitlab.com/Risgrynsgrot/diggy)
, at least for now. I didn't know where to take it, and I was so tired of
c++ that I didn't know what to do anymore.

At first I tried to move it to C (See the c_rework branch), but that went the
same as any other rework, which is not good. 

My next attempt was doing a game in Godot, becuase I felt like it would go
quicker having all the tools I need and not worrying so much about engine.
That didn't last for very long, I need to have more control than what I feel 
like I get from Godot. I think it's a good engine for certain types of game,
and you can probably get it to work for your needs, but I very quickly felt 
like I had to work around the engine rather than it helping me. Maybe I just
have very particular tastes.
